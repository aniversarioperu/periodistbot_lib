================
periodistbot_lib
================

.. image:: https://img.shields.io/travis/aniversarioperu/periodistbot_lib.svg
        :target: https://travis-ci.org/aniversarioperu/periodistbot_lib

.. image:: https://img.shields.io/pypi/v/periodistbot_lib.svg
        :target: https://pypi.python.org/pypi/periodistbot_lib


Periodismo de gatos
-------------------
Este es un bot escrito en Python que busca posts con vídeos sobre animalitos en
sitios virales, obtiene información adicional del vídeo en YouTube y prepara un
post para ser posteado en ``graciosoanimal.com``.


Características
---------------
* Extrae videos de la web <https://actualidad.rt.com/videoclub>
* Extrae videos de la web <http://9gag.com/tv>

TODO
----
* Agregar plugin para que postee el vídeo con info relevante en la web
  ``graciosoanimal.com``.

Instalación
-----------

Requiere un virtualenvironment con Python3. Clonar el repositorio e instalar
los requerimientos.

.. code-block:: shell

    mkvirtualenv -p /usr/bin/python3 periodistbot
    git clone https://aniversarioperu@bitbucket.org/aniversarioperu/periodistbot_lib.git
    cd periodistbot_lib
    pip install -r requirements/base.txt


.. image:: https://asciinema.org/a/29845.png
    :target: https://asciinema.org/a/29845

Configuración
-------------
Necesitas crear una cuenta de usuario en Wordpress para el bot. Poner la dirección
url del archivo ``xmlrpc.php``, usuario y contraseña necesarios. Todo debe estar
en un archivo llamado ``config.json``:


.. image:: https://asciinema.org/a/29846.png
    :target: https://asciinema.org/a/29846

Modo de uso
-----------
.. code-block:: shell

        python -m periodistbot https://actualidad.rt.com/
        python -m periodistbot http://9gag.com/tv/

Resultado
---------
.. image:: media/wordpress_post.png
