import dataset


def is_posted(story):
    """Check in database if story has been posted.

    Returns:
        - boolean

    """
    my_db = dataset.connect("sqlite:///periodistbot_db.sqlite3")
    table = my_db['gracioso_animal']
    if table.find_one(youtube_id=story['youtube_id']):
        return True
    else:
        return False

def flag_as_posted(story):
    """Write youtube video id in database and site that it was posted to.

    """
    my_db = dataset.connect("sqlite:///periodistbot_db.sqlite3")
    table = my_db['gracioso_animal']
    table.insert({
        'youtube_id': story['youtube_id'],
        'site': 'gracioso_animal',
    })
