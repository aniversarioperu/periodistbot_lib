import sys

from periodistbot import scrape_video
from . import wordpress
from . import db


def main():
    if len(sys.argv) < 2:
        print("Error. Enter homepage of viral portal with youtube videos as argument")
        print(" >>> python -m periodistbot https://actualidad.rt.com/videoclub\n\n")
        sys.exit(1)

    url = sys.argv[1].strip()
    stories = scrape_video(url)

    for story in stories:
        print("\nProcessing: {0}".format(story['title']))
        if not db.is_posted(story):
            print("Video has not been posted. Posting...")
            wordpress.post_to_wp(story['title'], story['content'])
            db.flag_as_posted(story)
        else:
            print("Video has been posted. Skipping...")


if __name__ == "__main__":
    main()
