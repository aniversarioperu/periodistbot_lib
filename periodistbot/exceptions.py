class WordPressNotConfigured(Exception):
    pass

class ViralSiteNotImplemented(Exception):
    pass
