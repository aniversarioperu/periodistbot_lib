import datetime
import math
import random

import humanize
import markdown
from textblob import TextBlob
from textblob.exceptions import TranslatorError

from .bot import is_video_of_animal


class Writer(object):
    """Writes stories based on metadata from youtube videos, only if the video
    is about animals.
    """
    def __init__(self, list_of_metadata):
        self.list_of_metadata = list_of_metadata
        self.stories = []

        self._write_stories()

    def _write_stories(self):
        for metadata in self.list_of_metadata:
            if is_video_of_animal(metadata):
                story_parts = dict()
                story_parts['title'] = self._make_title(metadata)
                story_parts['youtube_id'] = metadata['youtube_id']
                story_parts['first_paragraph'] = self._make_first_paragraph(metadata)
                story_parts['second_paragraph'] = self._make_second_paragraph(metadata)
                story_parts['third_paragraph'] = self._make_third_paragraph(metadata)
                story_parts['fourth_paragraph'] = self._make_fourth_paragraph()
                self.stories.append(self._assemble_story(story_parts))

    def _make_title(self, metadata):
        title = 'YouTube: '
        text = metadata['title']
        if 'actualidad.rt' in metadata['page_url']:
            text = text.replace('Videoclub – ', '')
            title += text
        else:
            title += self._translate(text)
        return self.proof_read(title)

    def _translate(self, text):
        blob = TextBlob(text)
        translated = False
        try:
            translated = str(blob.translate(to='es'))
        except TranslatorError as e:
            print("Error %s" % e)

        if translated is not False:
            return translated
        else:
            return text

    def proof_read(self, text):
        """
        `Voltea` nota y hace correcciones ortográficas.
        :param text:
        :return:
        """
        text = text.replace('video', 'vídeo')
        text = text.replace('Miercoles', 'Miércoles')
        return text

    def _make_first_paragraph(self, metadata):
        p = ''
        if 'description' in metadata:
            text = metadata['description']
            if "actualidad.rt" in metadata['page_url']:
                p += text
            else:
                p += self._translate(text)
        else:
            p = ''
        return self.proof_read(p)

    def _make_second_paragraph(self, metadata):
        views = metadata['views']
        if views > 1000:
            views = math.floor(views / 1000)*1000
        p = "La grabación fue compartida en Internet por la cuenta del usuario "
        p += metadata['author']
        p += " el pasado " + str(metadata['published_human_date'])
        p += " y ya superó más de " + humanize.intcomma(views) + " visitas"
        p += self.get_virality(views, metadata['published'])
        return self.proof_read(p)

    def get_virality(self, views, published_date):
        definitely_viral = [
            ', convirtiéndose en menos de una semana en un éxito viral.',
            ', habiéndose convertido en viral en el lapso de una semana.',
        ]
        possibly_viral = [
            ', siendo probable que se convierta en un vídeo viral en cuestión de días.',
            ', parece estar destinado a convertirse en viral.',
            ', sin lugar a dudas se convertirá en viral en los próximos días.'
        ]
        pub = datetime.datetime.strptime(published_date, '%Y-%m-%d %H:%M:%S').date()
        today = datetime.date.today()
        delta = today - pub
        out = ''
        if views > 500000 and delta.days < 8:
            out += random.choice(definitely_viral)
        elif views > 1000 and delta.days < 5:
            out += random.choice(possibly_viral)
        else:
            out += '.'
        return out

    def _make_third_paragraph(self, metadata):
        p = "El vídeo dura " + metadata['duration'] + " segundos y ha sido " \
             "rebotado ya por medios como RT en Español o Mashable, los cuales " \
             "tienen secciones especializadas en difundir curiosidades de este tipo."
        return self.proof_read(p)

    def _make_fourth_paragraph(self):
        p = "Difundir este tipo de vídeos"
        p += ' se ha convertido en una curiosa costumbre de los usuarios de ' \
             'YouTube.'
        return self.proof_read(p)

    @staticmethod
    def _assemble_story(story_parts):
        """
        :param story_parts: dict('title',
                                 'youtube_id',
                                 'first_paragraph',
                                 'second_paragraph',
                                 'third_paragraph',
                                 'fourth_paragraph')

        Returns:
            - dict : {'title': 'blah', 'content': 'blah blah'}
        """
        story_markdown = ""
        for key in ['title', 'youtube_id', 'first_paragraph', 'second_paragraph',
                    'third_paragraph', 'fourth_paragraph']:
            value = story_parts[key]
            if key == 'title':
                story_markdown += "#{0}\n\n".format(value)
            elif key == 'youtube_id':
                story_markdown += '<iframe src="https://www.youtube.com/embed/' \
                                  '{0}" width="850" height="510" frameborder="0" ' \
                                  'allowfullscreen="allowfullscreen"></iframe>"'.format(value)
            else:
                story_markdown += "{0}\n\n".format(value)

        html = markdown.markdown(story_markdown)
        return {
            'title': story_parts['title'].strip(),
            'content': html,
            'youtube_id': story_parts['youtube_id'],
        }
