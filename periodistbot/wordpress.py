import datetime

from slugify import slugify
from wordpress_xmlrpc import Client
from wordpress_xmlrpc import WordPressPost
from wordpress_xmlrpc.methods import posts

from . import config
from .exceptions import WordPressNotConfigured


def post_to_wp(title, content):
    """

    :param title: Title for post, used to guess post URL
    :param content: Body for post
    :return: post_url
    """
    msg = "\nYou need to set up your WordPress credentials in the file config.json: \n" \
          "Use:\n" \
          "    {" \
          "       \"wordpress_client\": \"https://mydomain.wordpress.com/xmlrpc.php\",\n" \
          "       \"wordpress_username\": \"yourusername\",\n" \
          "       \"wordpress_password\": \"yourpassword\"\n"  \
          "     }"

    if config.wordpress_client == '':
        raise WordPressNotConfigured(msg)
    if config.wordpress_username == '':
        raise WordPressNotConfigured(msg)
    if config.wordpress_password == '':
        raise WordPressNotConfigured(msg)

    wp = Client(config.wordpress_client, config.wordpress_username, config.wordpress_password)

    post = WordPressPost()
    post.title = title
    post.content = content
    post.id = wp.call(posts.NewPost(post))

    post.terms_names = {
        'post_tag': ['animalitos', 'graciosos'],
        'category': ['noticias']
    }
    post.post_status = 'publish'
    wp.call(posts.EditPost(post.id, post))

    return make_url(title)


def make_url(post_title):
    date_str = datetime.datetime.strftime(datetime.date.today(), '%Y/%m/%d/')

    post_url = config.wordpress_client.replace('xmlrpc.php', '')
    post_url += date_str
    post_url += slugify(post_title, max_length=200) + '/'
    return post_url
