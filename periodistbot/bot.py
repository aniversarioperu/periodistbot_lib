# -*- coding: utf-8 -*-
import re

import arrow
from bs4 import BeautifulSoup
import pafy
import requests
from requests.exceptions import MissingSchema

from .lists import list_animals
from . import exceptions


class VideoExtractorFromHomePages(object):
    """
    Extracts links of videos from viral site homepages.

    Parameters:
        url (str):   webaddress of viral site homepage that contains youtube videos

    Example:

        >>> from periodistbot.bot import VideoExtractorFromHomePages as VideoExtractor
        >>> vext = VideoExtractor("http://example.com")
        >>> vext.extract_video_urls()
        ["http://video1.html", "http://video2.html"]

    """
    def __init__(self, url):
        self.url = url
        self.html = None

    def _get_html_contents(self):
        r = requests.get(self.url)
        self.html = r.content

    def extract_video_urls(self):
        if 'actualidad.rt' in self.url:
            website = "actualidad.rt"
        elif 'mashable' in self.url:
            website = "mashable"
        elif '9gag' in self.url:
            website = "9gag"
        else:
            raise exceptions.ViralSiteNotImplemented("{0!r}".format(self.url))
        return self._extract_videos_from_website(website)

    def _extract_videos_from_website(self, website):
        if not self.html:
            self._get_html_contents()

        soup = BeautifulSoup(self.html, "html.parser")
        videos_urls = []
        append = videos_urls.append
        for i in soup.find_all('a'):
            if 'href' in i.attrs:
                this_link = i['href']

                if website == "mashable":
                    pattern = re.compile(
                        'http://mashable.com/' + '[0-9]{4}' + '/' + '[0-9]{2}' + '/' + '[0-9]{2}')
                elif website == "actualidad.rt":
                    pattern = re.compile("/videoclub/[0-9]+")
                elif website == "9gag":
                    pattern = re.compile("http://9gag.com/tv/p/\S+\S+")
                else:
                    raise exceptions.ViralSiteNotImplemented("{0!r}".format(self.url))

                res = re.search(pattern, this_link)

                if res and website == "mashable":
                    append(this_link)
                elif res and website == "actualidad.rt":
                    append('http://actualidad.rt.com' + this_link)
                elif res and website == "9gag":
                    append(this_link)
        return videos_urls


class VideoDataGetter(object):
    """
    Get metadata for youtube video from list of videos from viral webpage.

    Arguments:
        - page_urls (list):  list of URLs of video posts or filenames.

    Each Metadata, example:

        >>>
        ... {'author': 'ViralHog',
        ... 'description': 'En las imágenes se ve a Thongsri, un elefante de 17 años, que acude al rescate de su cuidador al ver que se encuentra en "peligro", en Tailandia.',
        ... 'duration': '00:38',
        ... 'image_url': 'https://cdn.rt.com/actualidad/public_images/2015.10/article/561a0f25c46188f70a8b45f3.jpg',
        ... 'keywords': ['animals', 'elephant', 'trainer', 'rescue', 'aid'],
        ... 'page_url': 'http://actualidad.rt.com/videoclub/188265-elefante-olvida-amigo-tailandia',
        ... 'published': '2015-10-07 22:17:01',
        ... 'published_human_date': 'Miércoles 7 de Octubre',
        ... 'title': 'Videoclub – Un elefante nunca olvida a un amigo',
        ... 'video_link_attrs': {'allowfullscreen': '',
        ... 'frameborder': '0',
        ... 'height': '100%',
        ... 'src': '//www.youtube.com/embed/HrBQliOsU2U',
        ... 'width': '100%'},
        ... 'views': 294745,
        ... 'youtube_id': 'HrBQliOsU2U'}

    Returns:
        List of metadata.
    """
    def __init__(self, page_urls):
        self.page_urls = page_urls
        self.video_metadata = self._get_metadata()

    def _get_metadata(self):
        video_metadata = []
        for page_url in self.page_urls:
            from_file = False

            try:
                r = requests.get(page_url)
            except MissingSchema:
                from_file = True

            if from_file:
                with open(page_url, "r") as handle:
                    content = handle.read()
            else:
                content = r.content

            metadata = {'page_url': page_url}
            soup = BeautifulSoup(content, "html.parser")
            for i in soup.find_all('meta'):
                if 'property' in i.attrs:
                    if 'og:title' in i.attrs['property']:
                        metadata['title'] = i.attrs['content']
                    if 'og:description' in i.attrs['property']:
                        metadata['description'] = i.attrs['content'].strip()
                    if 'og:image' in i.attrs['property']:
                        metadata['image_url'] = i.attrs['content'].strip()
            if 'mashable' in page_url or 'actualidad.rt' in page_url:
                youtube_embeded_link = self.extract_youtube_embeded_link(content, 'actualidad.rt')
            elif '9gag' in page_url:
                youtube_embeded_link = self.extract_youtube_embeded_link(content, '9gag')
            else:
                youtube_embeded_link = False

            if youtube_embeded_link:
                youtube_metadata = _get_youtube_metadata(youtube_embeded_link)
                if youtube_metadata:
                    metadata.update(youtube_metadata)
                video_metadata.append(metadata)


        return video_metadata

    @staticmethod
    def extract_youtube_embeded_link(content, site):
        """
        Arguments:
            - content (str):  html code
            - site (str):     either 9gag or other viral site

        Returns:
            - embedded link (str):  https://wwww.youtube.com/embed/LakjsfK

        """
        if site == '9gag':
            res = re.search('data-external-id="(.{11})', str(content))
        else:
            res = re.search("www\.youtube\.com/embed/(.{11})", str(content))

        if res:
            embedded_link = 'https://www.youtube.com/embed/{0}'.format(res.groups()[0])
        else:
            embedded_link = None
        return embedded_link


def _get_youtube_metadata(youtube_embedded_link):
    """
    Arguments:
        - youtube_embedded_link (str):  https://www.youtube.com/embed/kS6XJ544zxs

    Returns:
        - metadata (dict):  Contains key ``error`` if video no longer exists.
          Otherwise contains the following fields: ``author``, ``duration``,
          ``published``, ``published_human_date``, ``views``,
          ``youtube_description``, ``youtube_id``.

    """
    metadata = dict()
    metadata['youtube_id'] = _get_youtube_video_id(youtube_embedded_link)
    try:
        v = pafy.new(metadata['youtube_id'])
    except (OSError, ValueError, TypeError) as e:
        print("Error {0}".format(e))
        return {'error': e}
    metadata['author'] = v.author
    metadata['duration'] = re.sub('^00:', '', v.duration)
    metadata['views'] = v.viewcount
    try:
        metadata['youtube_description'] = v.description
    except KeyError as e:
        print("Error {0}".format(e))
        metadata['youtube_description'] = ''

    metadata['published'] = v.published
    published_human_date = arrow.get(v.published).format('dddd D MMMM', locale='es')
    published_human_date = re.sub(' ([aA-zA]+)$', ' de \\1', published_human_date)
    metadata['published_human_date'] = published_human_date

    return metadata


def _get_youtube_video_id(embedded_link):
    res = re.search('embed/(.{11})', embedded_link)
    if res:
        return res.groups()[0]
    else:
        return None


def is_video_of_animal(video_metadata):
    """Several checks to find out if this video is of animals, cat, dogs, etc.

    Arguments:
        - video_metadata (dict):
    """
    is_animal_video = False

    for key, metadata_value in video_metadata.items():
        for keyword in list_animals:
            if isinstance(metadata_value, list):
                metadata_value = ' '.join(metadata_value)
            elif isinstance(metadata_value, dict):
                metadata_value = ' '.join([v for k, v in metadata_value.items()])

            if re.search(" " + keyword + " ", str(metadata_value), flags=re.I):
                print("\n> Encontré vídeo de animalito: {0}\n".format(keyword))
                is_animal_video = True

    return is_animal_video


def get_video_data(page_url, html):
    """Parses a viral video page and extracts all metadata of video..

    Arguments:
        - page_url (str):
        - html (str):      content of html page

    """
    metadata = {'page_url': page_url}
    soup = BeautifulSoup(html, "html.parser")
    for i in soup.find_all('meta'):
        if 'property' in i.attrs:
            if 'og:title' in i.attrs['property']:
                metadata['title'] = i.attrs['content']
            if 'og:description' in i.attrs['property']:
                metadata['description'] = i.attrs['content'].strip()
            if 'og:image' in i.attrs['property']:
                metadata['image_url'] = i.attrs['content'].strip()
    if 'mashable' in page_url or \
            'actualidad.rt' in page_url or \
            '9gag' in page_url:
        iframe = soup.find('iframe')
        if iframe:
            embeded_link = iframe.attrs['src']
            if 'youtube' in embeded_link:
                metadata = _get_youtube_metadata(embeded_link)
                return metadata
            else:
                return "error - viral site not implemented"
