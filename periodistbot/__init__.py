# -*- coding: utf-8 -*-
from .bot import VideoDataGetter
from .bot import VideoExtractorFromHomePages
from .writer import Writer


__author__ = 'AniversarioPeru'
__email__ = 'yoni@aniversarioperu.me'
__version__ = '0.2.0'


def scrape_video(url):
    """
    Scrape video from webpage, which can be a portal of "news", about cats.

    Example:

        >>> url = 'http://mashable.com/category/viral-video/'
        >>> url = 'http://actualidad.rt.com/videoclub'
        >>> scrape_video(url)

    Returns:
        Metadata about the video as JSON which can be used to write a Wordpress blogpost.
    """
    all_video_urls = list(set(get_videos_urls(url)))

    video_data = VideoDataGetter(all_video_urls).video_metadata
    w = Writer(video_data)
    return w.stories


def get_videos_urls(url):
    extractor = VideoExtractorFromHomePages(url)
    return extractor.extract_video_urls()
