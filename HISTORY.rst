.. :changelog:

History
-------

0.2.0 (2015-12-28)
------------------

* Extracting videos from 9gag.
* Keeping track of posted videos to gracioso animal in sqlite database to avoid
  posting duplicates.

0.1.0 (2015-01-11)
------------------

* First release on PyPI.
