============
Installation
============

At the command line::

    $ easy_install periodistbot_lib

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv periodistbot_lib
    $ pip install periodistbot_lib
