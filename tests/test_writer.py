import unittest

from periodistbot.writer import Writer


metadata_elefante = {
    'author': 'ViralHog',
    'description': 'En las imágenes se ve a Thongsri, un elefante de 17 años, que acude al rescate de su cuidador al ver que se encuentra en "peligro", en Tailandia.',
    'duration': '00:38',
    'image_url': 'https://cdn.rt.com/actualidad/public_images/2015.10/article/561a0f25c46188f70a8b45f3.jpg',
    'keywords': ['animals', 'elephant', 'trainer', 'rescue', 'aid'],
    'page_url': 'http://actualidad.rt.com/videoclub/188265-elefante-olvida-amigo-tailandia',
    'published': '2015-10-07 22:17:01',
    'published_human_date': 'Miércoles 7 de Octubre',
    'title': 'Videoclub – Un elefante nunca olvida a un amigo',
    'video_link_attrs':
        {
            'allowfullscreen': '',
            'frameborder': '0',
            'height': '100%',
            'src': '//www.youtube.com/embed/HrBQliOsU2U',
            'width': '100%',
        },
    'views': 338076,
    'youtube_id': 'HrBQliOsU2U',
}


class TestWriter(unittest.TestCase):
    def setUp(self):
        self.writer = Writer([metadata_elefante])

    def test_making_title(self):
        self.assertEqual("YouTube: Un elefante nunca olvida a un amigo",
                         self.writer._make_title(metadata_elefante))

    def test_making_first_paragraph(self):
        self.assertEqual(
            'En las imágenes se ve a Thongsri, un elefante de 17 años, que '
            'acude al rescate de su cuidador al ver que se encuentra en '
            '"peligro", en Tailandia.',
            self.writer._make_first_paragraph(metadata_elefante))

    def test_making_second_paragraph(self):
        expected = 'La grabación fue compartida en Internet por la cuenta del usuario ' \
                   'ViralHog el pasado Miércoles 7 de Octubre y ya superó más de ' \
                   '338,000 visitas'
        result = self.writer._make_second_paragraph(metadata_elefante)
        self.assertTrue(result.startswith(expected))

    def test_making_third_paragraph(self):
        self.assertEqual(
            'El vídeo dura 00:38 segundos y ha sido rebotado ya por medios como '
            'RT en Español o Mashable, los cuales tienen secciones especializadas '
            'en difundir curiosidades de este tipo.',
            self.writer._make_third_paragraph(metadata_elefante))

    def test_making_fourth_paragraph(self):
        self.assertEqual(
            'Difundir este tipo de vídeos se ha convertido en una curiosa '
            'costumbre de los usuarios de YouTube.',
            self.writer._make_fourth_paragraph())
