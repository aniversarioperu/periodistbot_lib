#!-*- coding:utf-8 -*-
import os
import unittest

from periodistbot.bot import VideoDataGetter


DATA_FOLDER = os.path.abspath(os.path.join(os.path.dirname(__file__), 'Data'))


class TestVideoDataGetter(unittest.TestCase):
    def test_get_metadata_from_actualidad_rt(self):
        page_urls = [os.path.join(DATA_FOLDER, "actualidad.rt_karma.html")]
        vgetter = VideoDataGetter(page_urls)
        metadata = vgetter.video_metadata[0]
        self.assertEqual("02:46", metadata['duration'])
        self.assertTrue(metadata['title'].startswith("Videoclub – Con la comida no se juega"))
        self.assertTrue(metadata['description'].startswith("El conocido canal de YouTube"))
        self.assertEqual("06AZwFk5T_Y", metadata['youtube_id'])

    def test_get_metadata_from_9gag(self):
        page_urls = [os.path.join(DATA_FOLDER, "9gag_cats.html")]
        vgetter = VideoDataGetter(page_urls)
        metadata = vgetter.video_metadata[0]
        self.assertEqual("The gang leader really looks like a gang leader.",
                         metadata['description'])
