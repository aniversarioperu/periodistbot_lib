$(function() {
    /*
     * Cкрипт для каруселя
     * @author Айткулова Чинара
     */
    $.fn.carousel = function (options) {
        var defaults = {
            speed: 250,
            easing: "linear",
            slideStart: 0
        };

        var carousel = {};
        var el = this,
            prevGradient = null,
            nextGradient = null;
        
        var init =  function() {
            
            carousel.settings = $.extend({}, defaults, options);
            carousel.viewport = el.parent();
            carousel.list = carousel.settings.list;
            carousel.children = carousel.list.children();
            carousel.prev = carousel.settings.prev;
            carousel.next = carousel.settings.next;
            carousel.timer = null;
            
            setupLimits();
            
            // вешаем обработчики на кнопки вперед/назад
            carousel.prev.on({
                click: prev
            })
            
            carousel.next.on({
                click: next
            })
            
            // снимаем двойное выделение при двойном клике на кнопках
            carousel.next.add(carousel.prev).on("mousedown select", function() {
                return false;
            })
            
            el.on({
                scroll: checkLimit
            })
            
            $( window).on("orientationchange resize", function() {
                setupLimits();
            })
        }
        
        /*
         * Настройка слайдера:
         * 1. устанавливаем ширину списка carousel.list.width
         * 2. узнаем правый предел carousel.rightLimit
         * 3. устанавливаем длину шага carousel.moveStep
         */
        
        function setupLimits() {
            
            var height = 0;
            
            carousel.children.each(function() {
                height = Math.max( $(this).outerHeight(), height );
            });
            
            el.height(height + 30);
            carousel.viewport.height(height);
            
            carousel.moveStep = carousel.children.first().outerWidth(true);
            carousel.listMargin = Math.abs(parseInt(carousel.list.css("margin-left")) + parseInt(carousel.list.css("margin-right")));
            
            carousel.list.width(carousel.moveStep * carousel.children.length);
            
            carousel.rightLimit = carousel.list.width() - carousel.listMargin - el.width();
            
            if (carousel.settings.slideStart) {
                el.scrollLeft(carousel.settings.slideStart*carousel.moveStep);
            }
            
            if (carousel.next.closest(".gradient-right").length) {
                prevGradient = carousel.prev.closest(".gradient-left");
                nextGradient = carousel.next.closest(".gradient-right");
                
                prevGradient.add(nextGradient).on("mousedown select", function() {
                    return false;
                })
            }
            
            // Если в карусели меньше элементов чем ширина карусели, то функция ничегоне делаем.
            if (carousel.rightLimit <= 0) {
                (nextGradient || prevGradient) && (nextGradient.hide(), prevGradient.hide());
                return;
            } else {
                //показываем кнопку вперед, изначально он скрыт
                nextGradient && nextGradient.show();
            }

        }
        
        /*
         * проверяет пределы слайдера
         * если достигнут левый предел, то скрывает левую кнопку - назад
         * если достигнут правый предел, то скрывает правую кнопку - вперед
         * в противном случае отображает обе кнопки
         */
        function checkLimit() {
            var scroll = $(this).scrollLeft();
            
            if (scroll == 0) {
                carousel.prev.addClass("disabled");
                carousel.next.removeClass("disabled");
                prevGradient && (prevGradient.hide(150), nextGradient.show(150));
            } else if (scroll >= carousel.rightLimit) {
                carousel.prev.removeClass("disabled");
                carousel.next.addClass("disabled");
                nextGradient && (nextGradient.hide(150), prevGradient.show(200))
            } else {
                carousel.prev.removeClass("disabled");
                carousel.next.removeClass("disabled");
                nextGradient && (nextGradient.show(150), prevGradient.show(200));
            }
        }
        
        // показываем предыдущий элемент  или доскраливаем текущий элемент
        function prev() {
            
            var left = el.scrollLeft(),
                move = (left % carousel.moveStep > 0) ? left % carousel.moveStep : carousel.moveStep;
            
            el.animate(
                {scrollLeft : left - move},
                carousel.settings.speed, 
                carousel.settings.easing
            );
            
            return false;
        }
        
        // показываем следующий элемент или доскраливаем текущий элемент
        function next(){
            
            var left = el.scrollLeft(),
                move = (left % carousel.moveStep > 0) ? (carousel.moveStep - left % carousel.moveStep) : carousel.moveStep;
                
            el.animate(
                {scrollLeft : left + move},
                carousel.settings.speed, 
                carousel.settings.easing
            );
            
            return false;
        }
        
        return this.each(function() {
            init();
        })
    }
    
    var lastNews$ = $("#js-last-news"), leadVideos$ = $("#js-leadvideos");
    if(lastNews$.length) {
        lastNews$.find(".frame").carousel({
            list: lastNews$.find(".list"),
            prev: lastNews$.find(".prev"),
            next: lastNews$.find(".next")
        });
    }
    
    if (leadVideos$.length) {
        leadVideos$.find(".frame").carousel({
            list: leadVideos$.find(".list"),
            prev: leadVideos$.find(".prev"),
            next: leadVideos$.find(".next")
        });
    }
})