$(function() {
    /*
     * Cкрипт для скроллирования темы
     * @author Айткулова Чинара
     */
    $.fn.scrollLine = function (options) {
        
        var defaults = {
            speedInterval: 30
        };
        
        var line = {};
        var el = this;
        var isMobile =  /(Android|BlackBerry|phone|iPad|iPod|iPhone|IEMobile|Nokia|Mobile)/.test(navigator.userAgent);
        
        var init =  function() {
            
            line.settings = $.extend({}, defaults, options);
            line.viewport = el.parent();
            line.list = line.settings.list;
            line.children = line.list.children();
            line.prev = line.settings.prev;
            line.next = line.settings.next;
            line.timer = null;
            
            // Устанавливает настройки
            setup();
            
            /*
             * для мобильных версий оставляем нативный скролл,
             * а для дестопа обрабатываем кнопки вперед/назад
             */
            
            if (!isMobile) {
                if (line.prev.length || line.next.length) {
                    
                    var buttons = line.prev.add(line.next);
                    
                    buttons.on({
                      mouseenter: onMouseEnter,
                      mouseleave: onMouseLeave
                    })
                }
            }
            
            el.on({
                scroll: function () {
                    var scroll = $(this).scrollLeft();
                    // Если достигнут левый предел, то скрываем левую кнопку
                    if (scroll == 0) {
                        line.prev.length && (line.prev.hide(100), line.next.show(100), clearInterval(line.timer));
                    // Если достигнут правый предел, то скрываем правую кнопку
                    } else if (scroll >= line.rightLimit) {
                        line.next.length && (line.next.hide(100), line.prev.show(100), clearInterval(line.timer));
                    // в противном случае отображаем обе кнопки
                    } else {
                        (line.prev.length || line.next.length) && (line.prev.show(100), line.next.show(100));
                    }
                }
            });
            
            $( window).on("orientationchange resize", function() {
                setup();
            });
        };
        
        /*
         * Сработает когда курсор входит в пределы кнопок вперед/назад
         * 1. нужно знать в какую сторону скроллить и устанавливаем шаг для скролла
         * 2. запускаем интервальный таймер чтобы скроллить тему
         * - таймер вызывает функцию move и передает шаг
         * - через заданный интервал таймер снова вызывает move
         */
        
        function onMouseEnter(e) {
            e.preventDefault();
            var step;
            
            if (line.timer) {
                clearInterval(line.timer);
            }
            
            step = $(e.target).is(line.prev) ? -2 : 2;
            line.timer = setInterval(function(){move(step)}, line.settings.speedInterval);
        }
        
        /*
         * Сработает когда курсор покидает пределы кнопок вперед/назад
         * очищаем таймер
         */
        function onMouseLeave(e) {
            e.preventDefault();
            if(line.timer) {
                clearInterval(line.timer);
                line.timer = null;
            }
        }
        
        /*
         * Настройка темы:
         * 1. устанавливаем ширину/высоту списка
         * 2. узнаем правый предел line.rightLimit
         */
        function setup() {
            var width = 3;
           
            line.children.each(function(){
                width += $(this).outerWidth(true);
            })
            
            line.list.width(width);
            line.list.height(line.children.first().outerHeight(true));
            line.rightLimit = line.list.width() - el.width();
            
            // Если ширина списка Тем меньше родительской ширины, значит элементы не выходит за переделы родителя. функция завершает работу.
            if (el.width() >= line.list.width()) {
                line.next.length && line.next.hide();
                line.prev.length && line.prev.hide();
                return;
            } else {
                line.next.show();
            }
        }
        
       
        //добавляет шаг к значению скролла Темы
        function move (step) {
            var scroll = el.scrollLeft();
            el.scrollLeft(scroll + step);
        }
        
        return this.each(function() {
            init();
        });
    }
    
    var line = $(".js-actual-trends").find(".frame");
    if (line.length) {
        line.scrollLine({
            list: line.find("ul"),
            prev: $(".js-actual-trends").find(".gradient-left"),
            next: $(".js-actual-trends").find(".gradient-right")
        });
    }
})
