$(function(){
    
    /*
     * при разрешении < 684px работа хедера меняется на мобильный режим
     * - хедер фиксируется на самом вверху, когда пользователь скроллит документ
     *      когда пользователь отпускает клавишу/палец хедер плавно исчезает через N секунд
     *      при касании/скролла плавно появляется
     */
    
    function headerHandler() {
        
        var header = $(".js-header"),
            timer = null,
            doAction = false,
            infoBlock = header.find(".info-menu"),
            mainMenuList = header.find(".list"),
            searchBlock = header.find(".search"),
            win$ = $(window),
            dropDown = $(".icon-menu");
            
        var isMobile =  /(Android|BlackBerry|phone|iPad|iPod|iPhone|IEMobile|Nokia|Mobile)/.test(navigator.userAgent);
        
        if (header.length) {
            
            $(".js-mobile-icon").on("click", function(e){
               
                if ($(e.target).hasClass("menu-mobile-icon")) {
                    toggleClasses("showMainNav", "showSearch");
                } else {
                    toggleClasses("showSearch", "showMainNav");
                }
            })
            
            infoBlock.click(function (e) {
                if ($(e.target).hasClass("menu-mobile-icon") || $(e.target).hasClass("search-mobile-icon") || $(e.target).hasClass("search_input-txt")) return;
            })
            
            dropDown.click(function (e) {
                $(this).closest("li").toggleClass("show-subnav");
            })
            
            // Плавающий хедер
            if (!isMobile) {
                win$.on({
                    scroll: function () {
                        controllState();
                    },
                    
                    resize: function () {
                        if (!isMobile) {
                            if (infoBlock.is(":hidden") && (win$.scrollTop() < 61)) {
                                infoBlock.css("display", "block");
                            }
                        }
                    }
                })
                
                header.on({
                    mouseenter: function () {doAction = true;},
                    mouseleave: function () {doAction = false; controllState();}
                })
                
            } else {
                
                infoBlock.get(0).addEventListener(eventName("touchmove"), function () {
                    doAction = true;
                })
                
                infoBlock.get(0).addEventListener(eventName("touchend"), function () {
                    doAction = false;
                    controllState();
                })
                
                document.addEventListener(eventName("scroll"), function () { //Отслеживает событие "скролл"
                    infoBlock.fadeIn(100);
                    setTimeout(setTiming(2500));
                }, false);
                
                document.addEventListener(eventName("touchstart"), function () { //Отслеживает событие "тач"
                    infoBlock.fadeIn(100);
                }, false);
                
                if (window.navigator.msPointerEnabled || window.PointerEvent) {
                    
                    document.addEventListener(eventName("touchmove"), function () {
                            setTiming(2500);
                    }, false);
                    
                }
                
                document.addEventListener(eventName("touchend"), function () {  // Отслеживает окончание события "тач"
                    setTiming(2500);
                }, false);
            }
        }
        
        
        function toggleClasses(show, hide) {
            if (header.hasClass(show)) {
                header.removeClass(show);
            } else {
                header.addClass(show).removeClass(hide);
            }
        }
        
        function setTiming(interval) {
            timer && (clearTimeout(timer), timer = null);
            timer = setTimeout(function () {hideHeader(300)}, interval);
        }
        
        function controllState() {
            if (infoBlock.is(":visible")) {
                setTiming(1500);
            } else {
                infoBlock.fadeIn(100);
            }
        }
        
        function hideHeader(speed) {
            if (mainMenuList.is(":visible") || searchBlock.is(":visible")) return;
            
            if (win$.scrollTop() >= 45) {
                if (!doAction) {
                    infoBlock.fadeOut(speed);
                }
            }
        }
        
        function eventName(name) {
            var eName = "";
            
            if(('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
                return name;
            } else if (window.navigator.msPointerEnabled || window.PointerEvent){
                switch(name){
                    case "touchstart":
                        eName = "PointerDown";
                        break;
                    
                    case "touchend":
                        eName = "PointerUp";
                        break;
                    
                    case "touchmove":
                        eName = "PointerMove";
                        break;
                    
                    case "touchcancel":
                        eName = "PointerCancel";
                        break;
                }
                
                if (window.navigator.msPointerEnabled) {
                    return "MS"+eName;
                } else if (window.PointerEvent){
                    return eName.toLowerCase();
                }
            }
        }
    }
    
    headerHandler();
    
    // Скрипт для решения проблемы разной структуры меню
    // для различных версий
    
    var mobile_items = $('.main-menu__mobile-hide_js');
    
    $('#main-menu__secciones').click(function (e) {
        e.preventDefault();
        mobile_items.toggleClass('main-menu__item_open_js');
    });
 
});