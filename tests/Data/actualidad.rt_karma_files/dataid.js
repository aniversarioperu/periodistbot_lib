function serviceCount(){
    if ( typeof frontendCounter != "undefined" ) {
        var id = [];
          $('.js-dataid').not(".watches__counter_show").each(function() { 
            id.push($(this).attr('data-id')); 
          });
        $.ajax({
            type: "GET",
            dataType : "json",
            url: frontendCounter + "/" + id.join('-'),
            success: function(data) {
                // debugger;
                for (var i=0; i < data.message.length; i++) {
                    $('.watches__counter[data-id="'+data.message[i].publicId+'"]').html(data.message[i].total).addClass('watches__counter_show');
                }
            }
        });
    }
    if ( typeof frontendCounterHosturl != "undefined" ) {
        $.ajax({
            type: "GET",
            dataType : "text",
            url: frontendCounterHosturl
        });
     }
}
$(function () {
    serviceCount();
});