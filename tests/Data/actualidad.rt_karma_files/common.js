$(function(){
    
    //На странице ведущего его фото менятся с задержкой 5секунд
    function changePhotoLead() {
        var imgs = $("#js-picture").find("img"), current = 0;
        
        var nextImage = function() {
            if ( current >= imgs.length ) current = 0;
            
            imgs.eq( current++ ).fadeIn( function() {
                $( this ).delay( 5000 ).fadeOut( nextImage );
            })
        };
        
        nextImage();
    }
    
    if ($("#js-picture").length) changePhotoLead();
    
    
/* section programs */
    // icon-i > 480 
    $('.js-programs-info').click(function (e) {
    //     e.defaultPrevented;
        $("<div class='overlay active'></div>").appendTo($("body"));
        $(this).parents('.js-programs-list').addClass('active');        
    //     return false; 
    });
    $('.js-programs-list .js-programs-summary').click(function (e) {
    //     e.defaultPrevented;
        $('.js-programs-list').removeClass('active');
        $('.overlay.active').removeClass("active");
    //     return false; 
    });
/* end section programs */

// js-open 486px

function jsOPenMEd(){
    var dropDownMed = $('.js-open-med');
    if( $(window).width() < 685 ){
        dropDownMed.click(function(){                        
            if ( dropDownMed.parent().hasClass('active')) {
                $(this).parent().removeClass('active');
            } else {
                $(this).parent().addClass('active');
            }
        });
    }
}

$(window).resize(function(){
    jsOPenMEd();
});

jsOPenMEd();

$('.js-drop-down').click(function(){
    $('.js-open-close').addClass('active');         
});
$('.js-down-drop').click(function(){
    $('.js-open-close').removeClass('active');     
});

$('.js-open-menu .header').click(function(){
    if ($(this).parent().hasClass('active')) {
        $(this).parent().removeClass('active');
    } else {
        $(this).parent().addClass('active');
    }  
});
// end js-open 486px


    // $('.js-open-drop').click(function () {
    //     $(this).addClass('active');
    // });
});


/* Удаление дублирующих ссылок на статью в статьях  */

$(function(){

    $('.list__link_js').each( function() {

      if ( $(this).attr('href') == document.location.pathname ) {
          $( this ).parent('.list__item_js').remove();
      }
 
    });

}); 