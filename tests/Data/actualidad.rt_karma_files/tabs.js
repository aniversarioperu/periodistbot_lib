$(function(){
    function Tabs(params) {
        var self = this;
        this.opt = {
            tabs: null,
            changeBloks: null
        };
        
        this.params = $.extend({}, self.opt, params);
        
        this.onClick = function(tab){
            if( tab.hasClass("active")) return;
            tab.addClass("active").siblings().removeClass("active");
            self.params.changeBloks.eq(tab.index()).addClass("active").siblings().removeClass("active");
        };
        
        this.init = function(params){
            
            $(self.params.tabs).click(function(e){
                e.preventDefault();
                self.onClick($(this));
            });
            
        };
        
        this.init();
    }
   
    new Tabs({
        changeBloks: $(".most-populars .list-section"),
        tabs  : $(".most-populars .b-tabs li")
    });

    new Tabs({
        changeBloks: $(".article-comments .content"),
        tabs  : $(".article-comments .b-tabs li")
    });    
    // function switchMinWidthHight(){
    //     var x_width = $('.b-layout').width();
    //     if (x_width > 864){
    //         new Tabs({
    //             changeBloks: $(".most-popular_news .list"),
    //             tabs  : $(".most-popular_news .b-tabs li")
    //         });
    //     }    
    // } 
    // switchMinWidthHight();
    // $(window).resize(function(){
    //     switchMinWidthHight();
    // });      
});


