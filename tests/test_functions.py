import os
import unittest

from periodistbot.bot import _get_youtube_metadata
from periodistbot.bot import is_video_of_animal
from periodistbot.bot import get_video_data
from periodistbot.bot import _get_youtube_video_id


metadata_elefante = {
    'author': 'ViralHog',
    'description': 'En las imágenes se ve a Thongsri, un elefante de 17 años, que acude al rescate de su cuidador al ver que se encuentra en "peligro", en Tailandia.',
    'duration': '00:38',
    'image_url': 'https://cdn.rt.com/actualidad/public_images/2015.10/article/561a0f25c46188f70a8b45f3.jpg',
    'keywords': ['animals', 'elephant', 'trainer', 'rescue', 'aid'],
    'page_url': 'http://actualidad.rt.com/videoclub/188265-elefante-olvida-amigo-tailandia',
    'published': '2015-10-07 22:17:01',
    'published_human_date': 'Miércoles 7 de Octubre',
    'title': 'Videoclub – Un elefante nunca olvida a un amigo',
    'video_link_attrs':
        {
            'allowfullscreen': '',
            'frameborder': '0',
            'height': '100%',
            'src': '//www.youtube.com/embed/HrBQliOsU2U',
            'width': '100%',
        },
    'views': 338076,
    'youtube_id': 'HrBQliOsU2U',
}

DATA_FOLDER = os.path.abspath(os.path.join(os.path.dirname(__file__), 'Data'))


class TestSingleFunctions(unittest.TestCase):
    def setUp(self):
        self.maxDiff = None

    def test_get_youtube_metadata(self):
        youtube_embeded_link = "https://www.youtube.com/embed/8CKrkgnwNTk"
        result = _get_youtube_metadata(youtube_embeded_link)
        self.assertEqual("00:21", result['duration'])

    def test_is_video_of_animal(self):
        self.assertTrue(is_video_of_animal(metadata_elefante))

    def test_get_video_data_missing_video(self):
        """Test getting data from removed video"""
        with open(os.path.join(DATA_FOLDER, 'actualidad.rt_karma.html'), 'r') as handle:
            html = handle.read()
        self.assertTrue('error' in get_video_data("https://actualidad.rt.com/videoclub/185522", html))

    def test_get_youtube_video_id(self):
        embedded_link = "https://www.youtube.com/embed/JO3ZY3nfhws"
        self.assertEqual("JO3ZY3nfhws", _get_youtube_video_id(embedded_link))

        embedded_link = "https://www.youtube.com/watch?v=JO3ZY3nfhws"
        self.assertEqual(None, _get_youtube_video_id(embedded_link))
