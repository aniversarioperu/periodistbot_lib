import unittest

import dataset


class TestDb(unittest.TestCase):
    def setUp(self):
        self.story = {
            'content': '<h1>YouTube: Ver Harrison Ford Completamente Destruye Lego Halcón Milenario de un Fan | 9GAG.tv</h1>\n<iframe src="https://www.youtube.com/embed/3_FieJToFKc" width="850" height="510" frameborder="0" allowfullscreen="allowfullscreen"></iframe>\n\n<p>"Ouch ... Así doloroso ver ...</p>\n<p>La grabación fue compartida en Internet por la cuenta del usuario Team Coco el pasado Jueves 17 de Diciembre y ya superó más de 1,590,000 visitas.</p>\n<p>El vídeo dura 03:09 segundos y ha sido rebotado ya por medios como RT en Español o Mashable, los cuales tienen secciones especializadas en difundir curiosidades de este tipo.</p>\n<p>Difundir este tipo de vídeos se ha convertido en una curiosa costumbre de los usuarios de YouTube.</p>',
            'title': 'YouTube: Ver Harrison Ford Completamente Destruye Lego Halcón Milenario de un Fan | 9GAG.tv',
            'youtube_id': 'ajfljdf',
        }
        self.db = dataset.connect("sqlite:///test_db.sqlite3")

    def tearDown(self):
        self.db['gracioso_animal'].drop()

    def test_is_posted(self):
        table = self.db['gracioso_animal']
        self.assertIsNone(table.find_one(youtube_id=self.story['youtube_id']))

        table.insert({'youtube_id': self.story['youtube_id']})
        self.assertIsNotNone(table.find_one(youtube_id=self.story['youtube_id']))

