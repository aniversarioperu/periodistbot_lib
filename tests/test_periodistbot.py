#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import unittest

from bs4 import BeautifulSoup
import responses

from periodistbot.bot import VideoExtractorFromHomePages as VideoExtractor
from periodistbot.bot import VideoDataGetter as Vgetter
from periodistbot.exceptions import ViralSiteNotImplemented

DATA_FOLDER = os.path.abspath(os.path.join(os.path.dirname(__file__), 'Data'))


class TestPeriodistbot(unittest.TestCase):
    def setUp(self):
        self.video_getter = Vgetter([])

    @responses.activate
    def test_get_html_contents(self):
        url = "https://actualidad.rt.com/videoclub"
        responses.add(responses.GET, url, body="hola que ace", status=200,
                      content_type='application/text')
        resp = VideoExtractor(url)
        resp._get_html_contents()
        self.assertEqual("hola que ace", resp.html.decode('utf-8'))

    def test_extract_video_urls_from_not_implemented_site(self):
        url = "http://example.com"
        vext = VideoExtractor(url)
        self.assertRaises(ViralSiteNotImplemented, vext.extract_video_urls)

    def test_extract_video_urls_from_actualidad_rt(self):
        url = "https://actualidad.rt.com/videoclub"
        vext = VideoExtractor(url)
        with open(os.path.join(DATA_FOLDER, 'actualidad_rt_home.html'), 'r') as handle:
            vext.html = handle.read()
        self.assertEqual("http://actualidad.rt.com/videoclub/188265-elefante-olvida-amigo-tailandia",
                         vext.extract_video_urls()[0])

    def test_get_video_urls_from_9gag(self):
        url = "https://9gag.com/tv"
        vext = VideoExtractor(url)
        with open(os.path.join(DATA_FOLDER, '9gag_tv.html'), 'r') as handle:
            vext.html = handle.read()
        self.assertTrue("http://9gag.com/tv/p/aWWJQD/every-darn-day?ref=tcr" in vext.extract_video_urls())

    def test_extract_youtube_link_actualidad_rt(self):
        with open(os.path.join(DATA_FOLDER, 'actualidad.rt_karma.html'), "r") as handle:
            html = handle.read()
        result = self.video_getter.extract_youtube_embeded_link(html, 'actualidad.rt')
        self.assertEqual("https://www.youtube.com/embed/06AZwFk5T_Y", result)

    def test_extract_youtube_link_9gag(self):
        with open(os.path.join(DATA_FOLDER, '9gag_cats.html'), "r") as handle:
            html = handle.read()
        result = self.video_getter.extract_youtube_embeded_link(html, '9gag')
        self.assertEqual("https://www.youtube.com/embed/nx6EeN3GIok", result)
